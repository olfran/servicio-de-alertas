﻿using System;
using Logging;
using ScavConfiguration;
using Mailer;

namespace ServicioDeAlertas.Services
{
    /// <summary>
    /// Servicio que permite enviar correos electrónicos a partir de la 
    /// configuración obtenida del archivo web.config
    /// </summary>
    public class MailerService : IMailerService
    {
        private IScavConfigurationManager _scavConfig;
        private IMailer _mailer;
        
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }

        /// <summary>
        /// Lista de emails separados por coma a los que les llegará
        /// una copia del correo actual
        /// </summary>
        public string CC { get; set; }

        public MailerService(IScavConfigurationManager scavConfig, IMailer mailer)
        {
            _scavConfig = scavConfig;
            _mailer = mailer;
        }

        public bool Send()
        {
            _mailer.MailerConfig = GetMailerConfig();
            return _mailer.Send();
        }

        private MailerConfig GetMailerConfig()
        {
            string from = _scavConfig.Get("EMFrom");

            MailerConfig mailerConfig = new MailerConfig()
            {
                Body = Body,
                CC = CC,
                From = from,
                IsHtml = true,
                Port = int.Parse(_scavConfig.Get("EMPuerto")),
                Subject = Subject,
                To = To,
                User = from,
                Password = _scavConfig.Get("EMMurci"),
                Host = _scavConfig.Get("EMSmtp"),
                EnableSsl = _scavConfig.Get("EMEnableSSL").ToLower() == bool.TrueString.ToLower()
            };

            return mailerConfig;
        }

        public void AppendTo(string to)
        {
            if (!String.IsNullOrEmpty(to))
            {
                To += String.IsNullOrEmpty(To) ? to : String.Format(",{0}", to);
            }
        }
    }
}