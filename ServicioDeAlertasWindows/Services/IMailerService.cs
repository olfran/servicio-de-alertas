﻿namespace ServicioDeAlertas.Services
{
    public interface IMailerService
    {
        string Body { get; set; }
        string CC { get; set; }
        string Subject { get; set; }
        string To { get; set; }

        bool Send();

        /// <summary>
        /// Añade un email separados por coma, si el parámetro to está vacío se omite
        /// </summary>
        void AppendTo(string to);
    }
}
