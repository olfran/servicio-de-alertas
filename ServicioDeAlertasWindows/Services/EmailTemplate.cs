﻿namespace ServicioDeAlertas.Services
{
    /// <summary>
    /// {0} = Total documentos por caducar
    /// {1} = Días por vencer o días vencidos
    /// {2} = Head de la tabla
    /// {3} = Body de la tabla
    /// </summary>
    public static class EmailTemplate
    {
        public const string ScavEmailTemplate = @"
<!DOCTYPE html>
<html>
<head>
	<meta charset=""utf-8"">
	<title>Informaci&oacute;n de SCAV</title>
	<style type=""text/css"" media=""screen"">
		body {{
			font-family: Helvetica, Arial;
			color: #555;
		}}

		* {{
			font-size: 9pt;
		}}

		table {{
   			border: 1px solid #27ae60;
			border-collapse: collapse;
			cursor: default;
			font-size: 10pt;
		}}

		thead {{
			color: #333;
			background-color: #ecf0f1;
		}}

		tbody > tr:HOVER {{
			 background: rgba(26, 188, 156, 0.1);
		}}

		td {{
            border: 1px solid #333;
			padding: 5px;
		}}

		th {{
            border: 1px solid #333;
		}}

        .info {{
            color: #2980b9;
        }}

        .danger {{
            background: #f2eeee;
        }}
	</style>
</head>
<body>
<div>
<p>
Estimado usuario, SCAV le informa que usted tiene un total de: {0} documento(s) por
caducar dentro de: {1} d&iacute;a(s). A continuación se muestran los documentos
a punto de caducar.
</p>
<table>
	<thead>
{2}
	</thead>
	<tbody>
{3}
	</tbody>
</table>
<br>
<b class=""info"">
Este es un correo automático enviado por nuestro sistema, por favor no responda este mensaje. Gracias.
</b>
</div>
</body>
</html>";

    public const string ScavEmailTemplateCaducados = @"
<!DOCTYPE html>
<html>
<head>
	<meta charset=""utf-8"">
	<title>Informaci&oacute;n de SCAV</title>
	<style type=""text/css"" media=""screen"">
		body {{
			font-family: Helvetica, Arial;
			color: #555;
		}}

		* {{
			font-size: 9pt;
		}}

		table {{
   			border: 1px solid #27ae60;
			border-collapse: collapse;
			cursor: default;
			font-size: 10pt;
		}}

		thead {{
			color: #333;
			background-color: #ecf0f1;
		}}

		tbody > tr:HOVER {{
			 background: rgba(26, 188, 156, 0.1);
		}}

		td {{
            border: 1px solid #333;
			padding: 5px;
		}}

		th {{
            border: 1px solid #333;
		}}

        .info {{
            color: #2980b9;
        }}

        .danger {{
            background: #f2eeee;
        }}
	</style>
</head>
<body>
<div>
<p>
Estimado usuario, SCAV le informa que {0} documento(s) del cat&aacute;logo {1} ha(n) caducado.
A continuación se muestran los documentos caducados.</strong>
</p>
<table>
	<thead>
{2}
	</thead>
	<tbody>
{3}
	</tbody>
</table>
<br>
<b class=""info"">
Este es un correo automático enviado por nuestro sistema, por favor no responda este mensaje. Gracias.
</b>
</div>
</body>
</html>";

    }
}
