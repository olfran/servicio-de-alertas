﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Text;
using System.Linq;
using Logging;
using ScavConfiguration;
using ServicioDeAlertas.Alerts.Domain.Models;
using ServicioDeAlertas.Alerts.Infrastructure.Interfaces;
using ServicioDeAlertas.Alerts.Infrastructure.Repositories;
using ServicioDeAlertasWindows.Alerts;
using ServicioDeAlertasWindows.Alerts.Domain.Models;
using ServicioDeAlertasWindows.Container;

namespace ServicioDeAlertas.Services
{
    /// <summary>
    /// Servicio para verificar las alertas una por una
    /// Requiere un mailer service y un logger
    /// </summary>
    public class CheckAlertsService
    {
        private IScavConfigurationManager _config;
        private ILogger _logger;
        private IContainer _container;
        
        public CheckAlertsService(IContainer container)
        {
            _container = container;
            _config = container.GetConfigurationManager();
            _logger = container.GetLogger();
        }

        /// <summary>
        /// Verificar las alertas una por una y envia un correo a quien corresponda
        /// y escribe a los logs en caso de error
        /// </summary>
        public void CheckAlerts()
        {
            try
            {
                CheckAlertsStrategy();
            }
            catch (Exception ex)
            {
                _logger.Log(ex.ToString(), LogLevel.Error);
            }
        }

        private void CheckAlertsStrategy()
        {
            string connectionString = _config.Get("gConexion");
            IList<CatalogoAlertasVigencia> alertas;

            using (OleDbConnection oleDbConn = new OleDbConnection(connectionString))
            {
                oleDbConn.Open();

                ICatalogoAlertasVigenciaRepository cavRepository = new CatalogoAlertasVigenciaRepository(oleDbConn);
                IFormulario formulario = _container.GetFormulario(oleDbConn);
                alertas = cavRepository.GetAll();

                //Recorrer cada alerta y luego cada documento perteneciente al catálogo identificado en la alerta
                foreach (CatalogoAlertasVigencia alerta in alertas)
                {
                    try
                    {
                        ICatalogoRepository catalogoRepository = new CatalogoRepository(oleDbConn, alerta.ID_CATALOGO, alerta.ID_CAMPO_CAT_STR_FECHA, alerta.ID_CAMPO_CAT_STR, alerta.TERMINOS);
                        IEnumerable<FormularioPropiedades> formularioPropiedades = formulario.GetPropiedades(alerta.ID_CATALOGO);
                        IEnumerable<Catalogo> documentos = catalogoRepository.GetAll(formularioPropiedades);

                        NotifyStrategy(documentos, alerta, oleDbConn, formularioPropiedades);
                    }
                    catch(Exception ex)
                    {
                        _logger.Log(ex.ToString(), LogLevel.Error);
                    }
                }
            }
        }

        /// <summary>
        /// Verificar la vigencia de los documentos
        /// </summary>
        private void NotifyStrategy(IEnumerable<Catalogo> documentos, CatalogoAlertasVigencia alerta, OleDbConnection oleDbConn, IEnumerable<FormularioPropiedades> formularioPropiedades)
        {
            IMailerService mailerService = _container.GetMailerService();
            long vigencia1 = 0, vigencia2 = 0, vigencia3 = 0;
            int totalPorVencer1 = 0, totalPorVencer2 = 0, totalPorVencer3 = 0, totalVencidos = 0;
            mailerService.Subject = "Notificación de SCAV/AIRE (Alerta de Vigencia)";

            StringBuilder notificacion1 = new StringBuilder();
            StringBuilder notificacion2 = new StringBuilder();
            StringBuilder notificacion3 = new StringBuilder();
            StringBuilder notificacionVencidos = new StringBuilder();
            
            foreach (Catalogo documento in documentos)
            {
                if (documento.CAMPO_FECHA.HasValue)
                {
                    //Sólo si no ha vencido
                    if (DateTime.Now <= documento.CAMPO_FECHA.Value.AddDays(alerta.VIGENCIA))
                    {
                        TimeSpan tsVigencia = new TimeSpan(documento.CAMPO_FECHA.Value.AddDays(alerta.VIGENCIA).Ticks);
                        TimeSpan tsActual = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan tsRestante = tsVigencia.Subtract(tsActual);

                        if (tsRestante.Days == alerta.ANTICIPACION1 && !String.IsNullOrEmpty(alerta.RESPONSABLE1))
                        {
                            totalPorVencer1++;
                            notificacion1.AppendFormat(GetRow(alerta.NOMBRE_CATALOGO, documento.NUMREF, documento));
                            vigencia1 = alerta.ANTICIPACION1;
                        }

                        if (tsRestante.Days == alerta.ANTICIPACION2 && !String.IsNullOrEmpty(alerta.RESPONSABLE2))
                        {
                            totalPorVencer2++;
                            notificacion2.AppendFormat(GetRow(alerta.NOMBRE_CATALOGO, documento.NUMREF, documento));
                            vigencia2 = alerta.ANTICIPACION2;
                        }

                        if (tsRestante.Days == alerta.ANTICIPACION3 && !String.IsNullOrEmpty(alerta.RESPONSABLE3))
                        {
                            totalPorVencer3++;
                            notificacion3.AppendFormat(GetRow(alerta.NOMBRE_CATALOGO, documento.NUMREF, documento));
                            vigencia3 = alerta.ANTICIPACION3;
                        }
                    }

                    /*****************************************************************
                     * Nota:
                     * Este if es para enviar una notificación al usuario
                     * incluso cuando los documentos ya han vencido
                     *****************************************************************/
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (DateTime.Now >= documento.CAMPO_FECHA.Value.AddDays(alerta.VIGENCIA))
                    {
                        totalVencidos++;
                        notificacionVencidos.AppendFormat(GetRowVencido(alerta.NOMBRE_CATALOGO, documento.NUMREF, documento));
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
            }

            //Enviar el correo al responsable 1 del catálogo actual
            if (notificacion1.Length > 0)
            {
                mailerService.Body = String.Format(EmailTemplate.ScavEmailTemplate, totalPorVencer1, vigencia1, GetHead(formularioPropiedades), notificacion1.ToString());
                mailerService.To = alerta.RESPONSABLE1;
                mailerService.Send();
                _logger.Log(String.Format("(Responsable 1) Correo enviado exitosamente a: {0} - Alerta: {1}", mailerService.To, alerta.NOMBRE_ALERTA), LogLevel.Info);
            }

            if (notificacion2.Length > 0)
            {
                mailerService.Body = String.Format(EmailTemplate.ScavEmailTemplate, totalPorVencer2, vigencia2, GetHead(formularioPropiedades), notificacion2.ToString());
                mailerService.To = alerta.RESPONSABLE2;
                _logger.Log(String.Format("(Responsable 2) Correo enviado exitosamente a: {0} - Alerta: {1}", mailerService.To, alerta.NOMBRE_ALERTA), LogLevel.Info);
                mailerService.Send();
            }

            if (notificacion3.Length > 0)
            {
                mailerService.Body = String.Format(EmailTemplate.ScavEmailTemplate, totalPorVencer3, vigencia3, GetHead(formularioPropiedades), notificacion3.ToString());
                mailerService.To = alerta.RESPONSABLE3;
                mailerService.Send();
                _logger.Log(String.Format("(Responsable 3) Correo enviado exitosamente a: {0} - Alerta: {1}", mailerService.To, alerta.NOMBRE_ALERTA), LogLevel.Info);
            }

            //Notificar sobre los documentos vencidos
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (notificacionVencidos.Length > 0)
            {
                mailerService.AppendTo(alerta.RESPONSABLE1);
                mailerService.AppendTo(alerta.RESPONSABLE2);
                mailerService.AppendTo(alerta.RESPONSABLE3);

                if (!String.IsNullOrEmpty(mailerService.To))
                {
                    mailerService.Body = String.Format(EmailTemplate.ScavEmailTemplateCaducados, totalVencidos, alerta.NOMBRE_CATALOGO, GetHead(formularioPropiedades), notificacionVencidos.ToString());
                    mailerService.Send();
                    _logger.Log(String.Format("(Algunos vencidos) Correo enviado exitosamente a: {0} - Alerta: {1}", mailerService.To, alerta.NOMBRE_ALERTA), LogLevel.Info);
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        //@todo: Sería bueno crear un pequeño motor de plantillas en una clase aparte

        private string GetHead(IEnumerable<FormularioPropiedades> formularioPropiedades)
        {
            StringBuilder resultado = new StringBuilder();
            string head = "<tr style=\"color: #333;\"><th>Cat&aacute;logo</th><th>N&uacute;mero de Referencia</th></tr>";

            if (formularioPropiedades.Count() > 0)
            {
                resultado.Append("<tr>");
                foreach (var item in formularioPropiedades)
                {
                    resultado.AppendFormat("<td>{0}</td>", item.TituloCampo);
                }
                resultado.Append("</tr>");
            }
            else
            {
                resultado.Append(head);
            }

            return resultado.ToString();
        }


        /// <summary>
        /// Retorna una fila, sí no hay formulario email, retorna una fila por defecto con el
        /// nombre del catálogo y el numref
        /// </summary>
        private string GetRow(string nombreCatalogo, string numref, Catalogo documento)
        {
            StringBuilder resultado = new StringBuilder();
            string row = "<tr><td>{0}</td><td>{1}</td></tr>";

            if (documento.Campos.Count > 0)
            {
                resultado.Append("<tr>");
                foreach (var item in documento.Campos)
                {
                    resultado.AppendFormat("<td>{0}</td>", item.Value);
                }
                resultado.Append("</tr>");
            }
            else
            {
                resultado.AppendFormat(row, nombreCatalogo, numref);
            }

            return resultado.ToString();
        }

        /// <summary>
        /// Retorna una fila, sí no hay formulario email, retorna una fila por defecto con el
        /// nombre del catálogo y el numref
        /// </summary>
        private string GetRowVencido(string nombreCatalogo, string numref, Catalogo documento)
        {
            StringBuilder resultado = new StringBuilder();
            string row = "<tr style=\"background: #f2cece;\"><td>{0}</td><td>{1}</td></tr>";

            if (documento.Campos.Count > 0)
            {
                resultado.Append("<tr style=\"background: #f2cece;\">");
                foreach (var item in documento.Campos)
                {
                    resultado.AppendFormat("<td>{0}</td>", item.Value);
                }
                resultado.Append("</tr>");
            }
            else
            {
                resultado.AppendFormat(row, nombreCatalogo, numref);
            }

            return resultado.ToString();
        }
    }
}