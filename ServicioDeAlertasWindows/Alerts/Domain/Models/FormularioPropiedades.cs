﻿namespace ServicioDeAlertasWindows.Alerts.Domain.Models
{
    public class FormularioPropiedades
    {
        public long IdCampo { get; set; }
        public string NombreCampo { get { return string.Format("CAMPO_{0}", IdCampo); } }
        public string TituloCampo { get; set; }
    }
}
