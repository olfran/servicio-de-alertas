﻿using System;
using System.Collections.Generic;

namespace ServicioDeAlertas.Alerts.Domain.Models
{
    /// <summary>
    /// Modelo para representar un catálogo cualquiera y sus elementos comúnes
    /// </summary>
    public class Catalogo
    {
        public long ID_DOCUMENTO { get; set; }
        public string NUMREF { get; set; }
        public DateTime? FECHA_GENERACION { get; set; }
        public string QUETIENE { get; set; }

        //Campo fecha por defecto para determinar la vigencia del documento
        public DateTime? CAMPO_FECHA { get; set; }

        //Demás campos Diccionario<id, valor>
        public Dictionary<long, string> Campos { get; set; }

        public Catalogo()
        {
            Campos = new Dictionary<long, string>();
        }
    }
}