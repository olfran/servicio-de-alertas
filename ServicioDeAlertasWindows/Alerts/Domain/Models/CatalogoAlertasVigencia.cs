﻿using System;

namespace ServicioDeAlertas.Alerts.Domain.Models
{
    public class CatalogoAlertasVigencia
    {
        public long ID_CATALOGO_ALERTA { get; set; }
        public long ID_CAMPO_CAT_STR { get; set; }
        public string TERMINOS { get; set; }
        public long RECURRENTE { get; set; }
        public string NOMBRE_ALERTA { get; set; }
        public long ID_CATALOGO { get; set; }
        public long ID_CAMPO_CAT_STR_FECHA { get; set; }
        public long VIGENCIA { get; set; }
        public string RESPONSABLE1 { get; set; }
        public long ANTICIPACION1 { get; set; }
        public string RESPONSABLE2 { get; set; }
        public long ANTICIPACION2 { get; set; }
        public string RESPONSABLE3 { get; set; }
        public long ANTICIPACION3 { get; set; }
        public DateTime FECHA { get; set; }
        public string TIPO_RECURRENCIA { get; set; }
        public string NOMBRE_CATALOGO { get; set; }
    }
}
