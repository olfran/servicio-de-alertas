﻿using ServicioDeAlertasWindows.Alerts.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace ServicioDeAlertasWindows.Alerts
{
    public class FormularioEmail : IFormulario
    {
        private const int TipoFormularioEmail = 7;
        private OleDbConnection _connection;
        private const string SelectQuery = @"SELECT CATALOGO_ESTRUCTURA.ID_CAMPO_CAT_STR, CATALOGO_ESTRUCTURA.NOMBRE_ELEMENTO_CAT_STR FROM FORMULARIO_DETALLE JOIN FORMULARIOS ON FORMULARIOS.ID_FORMULARIO= FORMULARIO_DETALLE.ID_FORMULARIO JOIN CATALOGO_ESTRUCTURA ON CATALOGO_ESTRUCTURA.ID_CAMPO_CAT_STR= FORMULARIO_DETALLE.ID_CAMPO_CAT_STR WHERE FORMULARIOS.ID_CATALOGO={0} AND FORMULARIOS.TIPO_FORMULARIO={1} AND TIPO_ELEMENTO IN(0,1,2,3)";

        public FormularioEmail(IDbConnection connection)
        {
            _connection = connection as OleDbConnection;
        }

        public IEnumerable<FormularioPropiedades> GetPropiedades(long idCatalogo)
        {
            IList<FormularioPropiedades> resultado = new List<FormularioPropiedades>();
            string selectQuery = String.Format(SelectQuery, idCatalogo, TipoFormularioEmail);

            try
            {
                using (OleDbCommand oleCmd = new OleDbCommand(selectQuery, _connection))
                {
                    using (OleDbDataReader reader = oleCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            resultado.Add(new FormularioPropiedades()
                            {
                                IdCampo = TryParseLong(reader["ID_CAMPO_CAT_STR"].ToString()),
                                TituloCampo = reader["NOMBRE_ELEMENTO_CAT_STR"].ToString()
                            });
                        }
                    }
                }
            }
            catch
            {
                return new List<FormularioPropiedades>();
            }

            return resultado;
        }

        private long TryParseLong(string text)
        {
            long result = 0;
            long.TryParse(text, out result);

            return result;
        }
    }
}
