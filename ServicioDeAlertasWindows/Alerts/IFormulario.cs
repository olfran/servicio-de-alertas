﻿using ServicioDeAlertasWindows.Alerts.Domain.Models;
using System.Collections.Generic;

namespace ServicioDeAlertasWindows.Alerts
{
    public interface IFormulario
    {
        /// <summary>
        /// Retorna las propiedades básicas de un Formulario
        /// </summary>
        IEnumerable<FormularioPropiedades> GetPropiedades(long idCatalogo);
    }
}
