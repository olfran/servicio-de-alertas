﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using ServicioDeAlertas.Alerts.Domain.Models;
using ServicioDeAlertas.Alerts.Infrastructure.Interfaces;

namespace ServicioDeAlertas.Alerts.Infrastructure.Repositories
{
    /// <summary>
    /// Repositorio para la tabla CATALOGO_ALERTAS_VIGENCIA
    /// utiliza OleDb por lo que espera una conexión del tipo OleDb
    /// </summary>
    public class CatalogoAlertasVigenciaRepository : ICatalogoAlertasVigenciaRepository
    {
        private OleDbConnection _connection;

        private const string SelectQuery = @"SELECT * FROM CATALOGO_ALERTAS_VIGENCIA JOIN CATALOGO ON CATALOGO.ID_CATALOGO=CATALOGO_ALERTAS_VIGENCIA.ID_CATALOGO";

        public CatalogoAlertasVigenciaRepository(IDbConnection connection)
        {
            _connection = connection as OleDbConnection;
        }

        public IList<CatalogoAlertasVigencia> GetAll()
        {
            IList<CatalogoAlertasVigencia> result = new List<CatalogoAlertasVigencia>();

            using (OleDbCommand oleCmd = new OleDbCommand(SelectQuery, _connection))
            {
                using (OleDbDataReader reader = oleCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(MapToModel(reader));
                    }
                }
            }

            return result;
        }

        private CatalogoAlertasVigencia MapToModel(OleDbDataReader reader)
        {
            return new CatalogoAlertasVigencia()
            {
                ANTICIPACION1 = TryParseLong(reader["ANTICIPACION1"].ToString()),
                ANTICIPACION2 = TryParseLong(reader["ANTICIPACION2"].ToString()),
                ANTICIPACION3 = TryParseLong(reader["ANTICIPACION3"].ToString()),
                FECHA = TryParseDateTime(reader["FECHA"].ToString()),
                ID_CAMPO_CAT_STR = TryParseLong(reader["ID_CAMPO"].ToString()),
                ID_CAMPO_CAT_STR_FECHA = TryParseLong(reader["ID_CAMPO_FECHA"].ToString()),
                ID_CATALOGO = TryParseLong(reader["ID_CATALOGO"].ToString()),
                ID_CATALOGO_ALERTA = TryParseLong(reader["ID_CATALOGO_ALERTA"].ToString()),
                NOMBRE_ALERTA = reader["NOMBRE_ALERTA"].ToString(),
                RECURRENTE = TryParseLong(reader["RECURRENTE"].ToString()),
                RESPONSABLE1 = reader["RESPONSABLE1"].ToString(),
                RESPONSABLE2 = reader["RESPONSABLE2"].ToString(),
                RESPONSABLE3 = reader["RESPONSABLE3"].ToString(),
                TERMINOS = reader["TERMINOS"].ToString(),
                TIPO_RECURRENCIA = reader["TIPO_RECURRENCIA"].ToString(),
                VIGENCIA = TryParseLong(reader["VIGENCIA"].ToString()),
                NOMBRE_CATALOGO = reader["NOMBRE_CATALOGO"].ToString()
            };
        }

        private long TryParseLong(string text)
        { 
            long result = 0;
            long.TryParse(text, out result);

            return result;
        }

        private DateTime TryParseDateTime(string text)
        {
            DateTime result;
            DateTime.TryParse(text, out result);

            return result;
        }
    }
}
