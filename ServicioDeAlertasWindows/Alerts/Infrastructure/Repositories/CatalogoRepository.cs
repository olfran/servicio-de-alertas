﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using ServicioDeAlertas.Alerts.Domain.Models;
using ServicioDeAlertas.Alerts.Infrastructure.Interfaces;
using System.Text;
using System.Linq;
using ServicioDeAlertasWindows.Alerts.Domain.Models;

namespace ServicioDeAlertas.Alerts.Infrastructure.Repositories
{
    /// <summary>
    /// Repositorio para obtener datos de interés para el servicio
    /// de cualquier catálogo a partir de su id y el id del campo fecha.
    /// Utiliza OleDb por lo que espera una conexión del tipo OleDb
    /// </summary>
    public class CatalogoRepository : ICatalogoRepository
    {
        //Nombre de las columnas en la base de datos
        private string _catalogoName, _campoCatStrFechaName;
        private OleDbConnection _connection;
        private StringBuilder _selectQuery = new StringBuilder();
        private string _filter = "", _filterFieldName = "";

        public const string CatalogoPrefix = "CATALOGO_";
        public const string CampoPrefix = "CAMPO_";
        private string[] _listaFiltros;

        public CatalogoRepository(IDbConnection connection, long idCatalogo, long idCampoCatStrFecha)
        {
            Initialize(connection, idCatalogo, idCampoCatStrFecha);
        }

        //Este constructor permite filtrar campos
        public CatalogoRepository(IDbConnection connection, long idCatalogo, long idCampoCatStrFecha, long idFilter, string filter)
        {
            Initialize(connection, idCatalogo, idCampoCatStrFecha);

            /*
             * Filtra por la palabra exacta, los filtros deben estar separados por coma
             * Es importante que no existan espacios entre las comas o separadores, sin embargo
             * Puede existir espacio en los filtros, especialmente para nombres y apellidos
             * Por ejemplo, filtros correctos:
             * ACTIVO,INACTIVO,SIN ESTATUS
             * OLFRAN JIMENEZ,KEVIN MITNICK
             */
            if (!string.IsNullOrEmpty(filter))
            {
                _filter = filter;
                _filterFieldName = CampoPrefix + idFilter.ToString();
                _listaFiltros = _filter.Split(',');
                _selectQuery.Append("WHERE");
                for (int i = 0; i < _listaFiltros.Length; i++)
                {
                    _selectQuery.AppendFormat(" {0}=? OR ", _filterFieldName);
                }
                //Eliminar el último or
                _selectQuery.Remove(_selectQuery.Length - 4, 4);
            }
        }

        private void Initialize(IDbConnection connection, long idCatalogo, long idCampoCatStrFecha)
        {
            _catalogoName = CatalogoPrefix + idCatalogo.ToString();
            _campoCatStrFechaName = CampoPrefix + idCampoCatStrFecha;
            _connection = connection as OleDbConnection;
            _selectQuery.AppendFormat("SELECT * FROM {0} ", _catalogoName);
        }

        public IEnumerable<Catalogo> GetAll(IEnumerable<FormularioPropiedades> formularioPropiedades)
        {
            IList<Catalogo> result = new List<Catalogo>();

            using (OleDbCommand oleCmd = new OleDbCommand(_selectQuery.ToString(), _connection))
            {
                if (!String.IsNullOrEmpty(_filter))
                {
                    for (int i = 0; i < _listaFiltros.Length; i++)
                    {
                        string parametro = String.Format("@p{0}", i + 1);
                        oleCmd.Parameters.AddWithValue(parametro, _listaFiltros[i]);
                    }
                }

                using (OleDbDataReader reader = oleCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.Add(MapToModel(reader, formularioPropiedades));
                    }
                }
            }

            return result;
        }

        private Catalogo MapToModel(OleDbDataReader reader, IEnumerable<FormularioPropiedades> formularioPropiedades)
        {
            Catalogo catalogo = new Catalogo()
            {
                CAMPO_FECHA = TryParseDateTime(reader[_campoCatStrFechaName].ToString()),
                FECHA_GENERACION = TryParseDateTime(reader["FECHA_GENERACION"].ToString()),
                ID_DOCUMENTO = TryParseLong(reader["ID_DOCUMENTO"].ToString()),
                NUMREF = reader["NUMREF"].ToString(),
                QUETIENE = reader["QUETIENE"].ToString(),
            };

            if (formularioPropiedades.Count() > 0)
            {
                foreach (FormularioPropiedades item in formularioPropiedades)
                {
                    catalogo.Campos.Add(item.IdCampo, reader[item.NombreCampo].ToString());
                }
            }

            return catalogo;
        }

        private long TryParseLong(string text)
        {
            long result = 0;
            long.TryParse(text, out result);

            return result;
        }

        private DateTime? TryParseDateTime(string text)
        {
            DateTime result;
            if (!DateTime.TryParse(text, out result))
            {
                return null;
            }

            return result;
        }
    }
}