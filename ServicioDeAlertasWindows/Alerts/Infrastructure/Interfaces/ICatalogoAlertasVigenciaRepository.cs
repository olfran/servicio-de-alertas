﻿using ServicioDeAlertas.Alerts.Domain.Models;
using System.Collections.Generic;

namespace ServicioDeAlertas.Alerts.Infrastructure.Interfaces
{
    public interface ICatalogoAlertasVigenciaRepository
    {
        IList<CatalogoAlertasVigencia> GetAll();
    }
}