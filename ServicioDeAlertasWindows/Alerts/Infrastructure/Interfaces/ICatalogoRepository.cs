﻿using System.Collections.Generic;
using ServicioDeAlertas.Alerts.Domain.Models;
using ServicioDeAlertasWindows.Alerts.Domain.Models;

namespace ServicioDeAlertas.Alerts.Infrastructure.Interfaces
{
    public interface ICatalogoRepository
    {
        /// <summary>
        /// Retorna todos los documentos
        /// </summary>
        IEnumerable<Catalogo> GetAll(IEnumerable<FormularioPropiedades> formularioPropiedades);
    }
}
