﻿using System;
using System.IO;

namespace Logging.Backend
{
    public class ConsoleBackend : ILogBackend
    {
        public bool Log(string log, LogLevel level)
        {
            Console.WriteLine(log);
            return true;
        }
    }
}