﻿using Logging;
using Logging.Backend;
using Mailer;
using ScavConfiguration;
using ScavConfiguration.Interfaces;
using ScavConfiguration.Readers;
using ServicioDeAlertas.Services;
using ServicioDeAlertasWindows.Alerts;
using System.Data;

namespace ServicioDeAlertasWindows.Container
{
    /// <summary>
    /// Implementación de un contenedor muy simple, regresa instancias de las distintas 
    /// clases usadas en la aplicación a partir de interfaces
    /// </summary>
    public class DefaultContainer : IContainer
    {
        public ILogger GetLogger()
        {
            ILogBackend backend = new FileBackend(ServicioDeAlertasWindows.Properties.Settings.Default.LogsPath, true);
            ILogger logger = new Logger(backend);

            return logger;
        }
        
        public IScavConfigurationManager GetConfigurationManager()
        {
            IScavConfigurationReader configReader =
                new ScavWebConfigurationReader(Properties.Settings.Default.WebConfigPath);

            IScavConfigurationManager configManager =
                new ScavConfigurationManager(configReader);

            return configManager;
        }

        public IMailerService GetMailerService()
        {
            IMailer mailer = new DefaultMailer();

            IMailerService mailerService = 
                new MailerService(GetConfigurationManager(), mailer);

            return mailerService;
        }

        public IFormulario GetFormulario(IDbConnection connection)
        {
            IFormulario formulario = new FormularioEmail(connection);

            return formulario;
        }
    }
}