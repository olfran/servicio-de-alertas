﻿using Logging;
using ScavConfiguration;
using ServicioDeAlertas.Services;
using ServicioDeAlertasWindows.Alerts;
using System.Data;

namespace ServicioDeAlertasWindows.Container
{
    public interface IContainer
    {
        IScavConfigurationManager GetConfigurationManager();
        ILogger GetLogger();
        IMailerService GetMailerService();
        IFormulario GetFormulario(IDbConnection connection);
    }
}