﻿using System.ServiceProcess;

namespace ServicioDeAlertasWindows
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ServicioDeAlertas()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
