﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace ServicioDeAlertasWindows
{
    [RunInstaller(true)]
    public partial class AlertsServiceInstaller : System.Configuration.Install.Installer
    {
        public AlertsServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
