﻿using System.Collections.Generic;
namespace Mailer
{
    public class MailerConfig
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        /// <summary>
        /// Lista de emails separados por comas a los que les llegará
        /// una copia del correo
        /// </summary>
        public string CC { get; set; }
        /// <summary>
        /// Indica si el mensaje contiene código HTML
        /// </summary>
        public bool IsHtml { get; set; }
        public int Port { get; set; }

        public string User { get; set; }
        public string Password { get; set; }

        public string Host { get; set; }

        public bool EnableSsl { get; set; }
    }
}
