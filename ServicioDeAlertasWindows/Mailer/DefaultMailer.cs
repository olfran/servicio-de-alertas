﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Mailer
{
    /// <summary>
    /// Clase para enviar correos a partir de una configuración específicada
    /// por medio de la clase MailerConfig
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>
    public class DefaultMailer : IMailer
    {
        MailerConfig _config;

        public MailerConfig MailerConfig
        { 
            get { return _config; }
            set { _config = value; }
        }

        public DefaultMailer() { }

        public DefaultMailer(MailerConfig config)
        {
            _config = config;
        }

        public bool Send()
        {
            MailMessage mailMessage = GetMailMessage();
            SmtpClient smtpClient = GetSmtpClient();

            try
            {
                smtpClient.Send(mailMessage);
            }
            catch (Exception)
            {
                return false;
            }

            mailMessage.Dispose();

            return true;
        }

        private SmtpClient GetSmtpClient()
        {
            SmtpClient smtpClient = new SmtpClient()
            {
                EnableSsl = _config.EnableSsl,
                Port = _config.Port,
                Credentials = new NetworkCredential(_config.User, _config.Password),
                Host = _config.Host
            };

            return smtpClient;
        }

        private MailMessage GetMailMessage()
        {
            MailMessage mailMessage =
                new MailMessage(_config.From, _config.To.Replace(";", ","), _config.Subject, _config.Body);

            mailMessage.IsBodyHtml = _config.IsHtml;
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (!string.IsNullOrEmpty(_config.CC))
            {
                mailMessage.CC.Add(_config.CC);
            }

            if (!String.IsNullOrEmpty(_config.CC))
            {
                mailMessage.CC.Add(_config.CC.Replace(";", ","));
            }

            return mailMessage;
        }
    }
}