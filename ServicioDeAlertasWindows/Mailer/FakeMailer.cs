﻿using System;
using System.IO;
using System.Text;

namespace Mailer
{
    /// <summary>
    /// Clase para enviar correos ficticios en la consola y en un archivo html, ideal
    /// para hacer pruebas y no saturar el servidor de correos
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>
    public class FakeMailer : IMailer
    {
        MailerConfig _config;

        public MailerConfig MailerConfig
        {
            get { return _config; }
            set { _config = value; }
        }

        public FakeMailer() { }

        public FakeMailer(MailerConfig config)
        {
            _config = config;
        }

        public bool Send()
        {
            string fileName = Path.Combine("correos", Guid.NewGuid().ToString() + ".html");
            StringBuilder html = new StringBuilder();

            Directory.CreateDirectory("correos");

            html.AppendFormat("<strong>De:</strong> {0}<br>", _config.From);
            html.AppendFormat("<strong>Para:</strong> {0}<br>", _config.To);
            html.AppendFormat("<strong>CC:</strong> {0}<br>", _config.CC);
            html.AppendFormat("<strong>Subject:</strong> {0}<br><br>", _config.Subject);
            html.AppendFormat("{0}", _config.Body);

            File.WriteAllText(fileName, html.ToString());

            return true;
        }
    }
}