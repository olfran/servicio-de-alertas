﻿namespace Mailer
{
    public interface IMailer
    {
        MailerConfig MailerConfig { get; set; }
        bool Send();
    }
}
