﻿using System.ServiceProcess;
using System.Timers;
using ServicioDeAlertas.Services;
using ServicioDeAlertasWindows.Container;
using System.Threading.Tasks;

namespace ServicioDeAlertasWindows
{
    /// <summary>
    /// Este servicio permite gestionar el sistema de alertas de SCAV
    /// verifica las alertas correspondientes en la tabla CATALOGO_ALERTAS_VIGENCIA
    /// y envía un correo electrónico dependiendo de los días de anticipación
    /// configurados.
    /// 
    /// El código, en su mayoría, se encuentra estructurado en forma de componentes
    /// para que sea más fácil su mantenimiento o extensión.
    /// 
    /// Se ejecuta con una frecuencia configurable -en minutos- (Mediante un Timer)
    /// Si ocurre algún error se generan los logs correspondientes y se sigue
    /// ejecutando el servicio con total normalidad.
    /// 
    /// El envío de mails es un proceso lento, puede tardarse más de tres segundos
    /// por correo, dependiendo de la latencia de la red y el proceso se bloquea
    /// momentaneamente mientras ocurre el envío, se pudo haber utilizado async,
    /// pero el método SendAsync aunque no bloquea el proceso, de todas formas
    /// hay que esperar a que finalice para poder enviar otro correo.
    /// 
    /// Nota: Si en el futuro, el proceso de verificación y envío de alertas resulta
    /// lento Lo ideal sería un sistema de colas, como MSMQ o RabbitMQ para enviar
    /// los correos.
    /// 
    /// Por Olfran Jiménez <olfran@gmail.com>
    /// </summary>

    partial class ServicioDeAlertas : ServiceBase
    {
        private System.Timers.Timer _timer;
        
        public ServicioDeAlertas()
        {
            InitializeComponent();

            int interval = ServicioDeAlertasWindows.Properties.Settings.Default.Interval;

            //Tiempo de espera en minutos para chequear las alertas
            _timer = new System.Timers.Timer(1000 * 60 * interval);
            _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckAlerts();
        }

        void CheckAlerts()
        {
            IContainer container = new DefaultContainer();
            CheckAlertsService alertsService = new CheckAlertsService(container);

            alertsService.CheckAlerts();
        }

        protected override void OnStart(string[] args)
        {
            Task.Factory.StartNew(() =>
            {
                CheckAlerts();
            });

            _timer.Start();
        }

        protected override void OnStop()
        {
            _timer.Stop();
        }
    }
}